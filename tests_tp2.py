from tp2 import *

# on veut pouvoir creer des boites
def test_box_create():
    b = Box()

# on veut pouvoir mettre des trucs dans les boites
def test_box_add():
    b = Box()
    b.add("Pom'Potes")
    b.add("Cookie")
    assert "Pom'Potes" in b._contents
    assert not "Crepe" in b._contents

# on veut savoir si un truc est dans une boite
def test_box__contains__():
    b = Box()
    b.add("Pom'Potes")
    b.__contains__("Pom'Potes")
    b.__contains__("Cookie")
    assert b.__contains__("Pom'Potes")
    assert not b.__contains__("Cookie")

# on veut enlever un truc d'une boite
def test_box_remove():
    b = Box()
    b.add("Pom'Potes")
    assert "Pom'Potes" in b._contents
    b.remove("Pom'Potes")
    assert not "Pom'Potes" in b._contents

# on veut savoir si une boite est ouverte ou non, l'ouvrir et la fermer
def test_box_isOpen():
    b = Box()
    assert not b.isOpen()
    b.open()
    assert b.isOpen()
    b.close()
    assert not b.isOpen()


# on veut regarder ce qu'il y a dans une boite
def test_box_actionLook():
    b = Box()
    assert b.actionLook() == "La boite est fermee."
    b.open()
    b.add("Pom'Potes")
    assert b.actionLook() == "La boite contient: Pom'Potes."
    b.add("Cookie du guerrier")
    assert b.actionLook() == "La boite contient: Pom'Potes, Cookie du guerrier."

# on veut creer des choses qui prennent de la place
def test_thing_create():
    t = Thing(3)

# on veut connaitre la place prise par une chose
def test_thing_volume():
    t = Thing(3)
    assert t.volume() == 3

# on veut donner une capacite a une boite
def test_box_setCapacity():
    b = Box()
    assert b.capacity() == None
    b.setCapacity(749)
    assert b.capacity() == 749

# on veut tester si une boite a de la place pour une nouvelle chose
def test_box_hasRoomFor():
    b = Box()
    assert b.hasRoomFor(Thing(50))
    b.setCapacity(749)
    assert not b.hasRoomFor(Thing(5000))

# on veut mettre une chose dans une boite
def test_box_actionAdd():
    b = Box()
    assert not b.actionAdd(Thing(1))
    b.open()
    assert b.actionAdd(Thing(9000))
    b.setCapacity(749)
    assert not b.actionAdd(Thing(1000))
    assert b.actionAdd(Thing(500))
    assert not b.actionAdd(Thing(500))

# on veut donner un nom (qui ressemble a quelque chose) a une chose
def test_thing_setName():
    t = Thing(5)
    assert t.name == None
    t.setName("Brownie de la vie")
    assert t.name == "Brownie de la vie"

# on veut savoir si une chose a un nom
def test_thing_hasName():
    t = Thing(1104)
    assert not t.hasName()
    t.setName("BD Cake")
    assert t.hasName()

# on veut trouver un objet dans une boite
def test_box_find():
        b = Box()
        assert b.find("Motivation") == None
        t = Thing(50)
        t.setName("Cookie")
        b.open()
        b.actionAdd(t)
        assert b.find("Cookie") == "Cookie"
        b.close()
        assert b.find("Cookie") == None

# on veut pouvoir donner un nom a une chose des la creation (optionnel)
def test_thing_create_update():
    t = Thing(153, "Dormir")

# on veut pouvoir dire si la boite est ouverte ou non et sa capacite des la creation (optionnels)
def test_box_create_update():
    b = Box(True, 317)

def test_calcul():
    assert calcul()
    assert vrai_calcul()
