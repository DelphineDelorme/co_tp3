class Box:
    def __init__(self, is_open=False, capacity=None):
        self._contents = []
        self.is_open = is_open
        self.cap = capacity

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, truc):
        return truc in self._contents

    def remove(self, truc):
        self._contents.remove(truc)

    def isOpen(self):
        return self.is_open

    def open(self):
        self.is_open = True

    def close(self):
        self.is_open = False

    def actionLook(self):
        if not self.is_open:
            return "La boite est fermee."
        return "La boite contient: "  + ", ".join(self._contents) + "."

    def setCapacity(self, cap):
        self.cap = cap

    def capacity(self):
        return self.cap

    def hasRoomFor(self, thing):
        if self.cap == None:
            return True
        return self.cap - thing.volume() >= 0

    def actionAdd(self, thing):
        if not self.isOpen() or not self.hasRoomFor(thing):
            return False
        else:
            self.add(thing)
            if self.cap != None:
                self.cap -= thing.volume()
            return True

    def find(self, name):
        if not self.isOpen():
            return None
        for thing in self._contents:
            if thing.name == name:
                return thing.name
        return None



class Thing:
    def __init__(self, poids, name=None):
        self.poids = poids
        self.name = name

    def __repr__(self):
        return self.name

    def volume(self):
        return self.poids

    def setName(self, nom):
        self.name = nom

    def hasName(self):
        if self.name == None:
            return False
        return True




def calcul():
    return True

def vrai_calcul():
    return 1+1==2
